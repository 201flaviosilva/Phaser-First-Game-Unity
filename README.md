# Phaser First Game

## Description

Recriar o tutorial para desenvolver um jogo em Phaser, mas na Unity.

## Links and References

- [Play](https://play.unity.com/mg/other/phaser-first-game-in-unityl);
- [Code](https://gitlab.com/201flaviosilva/Phaser-First-Game-Unity);
- [Making Your First Phaser 3](https://phaser.io/tutorials/making-your-first-phaser-3-game-portuguese);
- [Example (Phaser)](https://phaser.io/examples/v3/view/games/firstgame/part9);
