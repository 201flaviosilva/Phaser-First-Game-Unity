﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class GameManager :MonoBehaviour {
    [SerializeField] Transform[] spawnStarPoints;
    [SerializeField] GameObject starPrefab;
    [SerializeField] GameObject bombPrefab;
    [SerializeField] Text scoreLabel;

    private int numStarsLeft = 0;

    private int score = 0;

    private void Start() {
        SpawnStars();
    }

    private void SpawnStars() {
        for (int i = 0; i < spawnStarPoints.Length; i++) {
            Instantiate(starPrefab, spawnStarPoints[i]);
        }
        numStarsLeft = spawnStarPoints.Length;
    }

    private void SpawnBomber() {
        Vector3 randomPosition = new Vector3(Random.Range(-4, 4), 0, Random.Range(-3, 3));
        var newBomb = Instantiate(bombPrefab, randomPosition, Quaternion.identity);
        Rigidbody2D _rigidbody2D = newBomb.GetComponent<Rigidbody2D>();
        _rigidbody2D.velocity = new Vector2(Random.Range(-10.0f, 10.0f), Random.Range(-10.0f, 10.0f));
    }

    public void CatchedAStar(GameObject star) {
        Destroy(star);
        numStarsLeft--;
        score++;
        scoreLabel.text = "" + score;
        if (numStarsLeft == 0) {
            SpawnStars();
            SpawnBomber();
        }
    }

    public void Dead() {
        Time.timeScale = 0f;
    }
}
