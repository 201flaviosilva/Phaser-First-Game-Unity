﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player :MonoBehaviour {
    private Rigidbody2D _rigidbody2D;
    private Animator _animatorController;

    [SerializeField] float speed;
    [SerializeField] float jumpForce;
    bool flipX = false;

    // Get Input Keys
    private float inputX = 0;
    private float isJumpInputPress = 0;

    private bool isInPlatform = false;

    [SerializeField] GameObject _gameManagerObject;
    private GameManager _gameManager;

    // Tint Dead
    private SpriteRenderer _spriteRender;
    [SerializeField] private Color32 deadColor = new Color32(1, 1, 1, 1); // Red (changed in inspector)

    private void Awake() {
        _rigidbody2D = GetComponent<Rigidbody2D>();
        _animatorController = GetComponent<Animator>();
        _gameManager = _gameManagerObject.GetComponent<GameManager>();
        _spriteRender = GetComponent<SpriteRenderer>();
    }

    private void Update() {
        inputX = Input.GetAxis("Horizontal");
        isJumpInputPress = Input.GetAxis("Jump");

        if (inputX < 0) {
            _animatorController.Play("Run");
            if (flipX)
                toggleFlipX();
        } else if (inputX > 0) {
            _animatorController.Play("Run");

            if (!flipX)
                toggleFlipX();
        } else {
            _animatorController.Play("Idle");
        }
    }

    private void FixedUpdate() {
        // Move X Axis
        _rigidbody2D.velocity = new Vector2(inputX * speed, _rigidbody2D.velocity.y);


        // Jump
        if (isJumpInputPress > 0 && isInPlatform) {
            _rigidbody2D.velocity = new Vector2(_rigidbody2D.velocity.x, jumpForce);
        }
    }

    private void toggleFlipX() {
        flipX = !flipX;
        Vector3 scaler = transform.localScale;
        scaler.x *= -1;
        transform.localScale = scaler;
    }

    private void OnCollisionEnter2D(Collision2D other) {
        if (other.gameObject.tag == "Star") {
            _gameManager.CatchedAStar(other.gameObject);
        } else if (other.gameObject.tag == "Bomb") {
            _gameManager.Dead();
            _spriteRender.color = deadColor;
        }
    }

    private void OnTriggerEnter2D(Collider2D other) {
        if (other.tag == "Platform") {
            isInPlatform = true;
        }
    }

    private void OnTriggerExit2D(Collider2D other) {
        if (other.tag == "Platform") {
            isInPlatform = false;
        }
    }
}
